<?php

namespace App\Command;

use Symfony\Component\Console\Input\InputArgument;

class ImportAreaCommand extends AbstractImportCommand
{
    protected $title = "Import areas";

    protected function configure()
    {
        $this
            ->setName('import:area')
            ->setDescription('Imports the CSV file for areas')
            ->addArgument("file", InputArgument::OPTIONAL, "file to import", '../data/areas.csv');
    }

    function getQuery()
    {
        $sql = "REPLACE INTO area (id, name) VALUES(:id, :name)";
        return $this->em->getConnection()->prepare($sql);
    }

    function mapRow(array $header, array $row): array
    {
        return [
            'id' => $row[$header['id']],
            'name' => $row[$header['name']],
        ];
    }
}