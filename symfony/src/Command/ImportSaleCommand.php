<?php

namespace App\Command;

use Symfony\Component\Console\Input\InputArgument;

class ImportSaleCommand extends AbstractImportCommand
{
    protected $title = "Import sales";

    protected function configure()
    {
        $this
            ->setName('import:sale')
            ->setDescription('Imports the CSV file for sales')
            ->addArgument("file", InputArgument::OPTIONAL, "file to import", '../data/sales.csv');
    }

    function getQuery()
    {
        $sql = "REPLACE INTO sale (id, area_id, address_id, price, date) VALUES(:id, :area_id, :address_id, :price, :date)";
        return $this->em->getConnection()->prepare($sql);
    }

    function mapRow(array $header, array $row): array
    {
        return [
            'id' => $row[$header['id']],
            'area_id' => $row[$header['areaId']],
            'address_id' => $row[$header['addressId']],
            'price' => $row[$header['price']],
            'date' => $row[$header['date']],
        ];
    }
}