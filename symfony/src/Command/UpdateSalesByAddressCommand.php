<?php

namespace App\Command;

class UpdateSalesByAddressCommand extends AbstractUpdateCommand
{
    protected $title = "Update sales change for addresses";

    protected function configure()
    {
        $this
            ->setName('update:sales-by-address')
            ->setDescription('Updates sales change for addresses');
    }

    function getQuery()
    {
        $sql = "REPLACE INTO saleByAddress (address_id, fromDate, toDate, timespan, percentageChange) VALUES(:address_id, :fromDate, :toDate, :timespan, :percentageChange)";
        return $this->em->getConnection()->prepare($sql);
    }

    function getData():array
    {
        $connection = $this->em->getConnection();
        $connection->executeQuery("DELETE FROM saleByAddress");
        return $connection->fetchAll(
            <<<SQL
SELECT
    address_id,
    periods.toDate,
    periods.fromDate,
    (SELECT price from sale WHERE address_id = periods.address_id AND date=periods.fromDate) AS firstPrice,
    (SELECT price from sale WHERE address_id = periods.address_id AND date=periods.toDate) AS lastPrice
FROM sale LEFT JOIN address ON address.id=sale.address_id INNER JOIN(
    SELECT address_id, MIN(date) AS fromDate, MAX(date) AS toDate FROM sale GROUP BY address_id
) AS periods USING(address_id) GROUP BY address_id
SQL
        );
    }

    function mapRow(array $row): array
    {
        return [
            'address_id' => $row['address_id'],
            'fromDate' => $row['fromDate'],
            'toDate' => $row['toDate'],
            'timespan' => date_diff(date_create($row['toDate']), date_create($row['fromDate']))->format('%a'),
            'percentageChange' => ($row['lastPrice'] - $row['firstPrice']) / $row['firstPrice'] * 100,
        ];
    }
}