<?php

namespace App\Command;

use Symfony\Component\Console\Input\InputArgument;

class ImportAddressCommand extends AbstractImportCommand
{
    protected $title = "Import addresses";

    protected function configure()
    {
        $this
            ->setName('import:address')
            ->setDescription('Imports the CSV file for addresses')
            ->addArgument("file", InputArgument::OPTIONAL, "file to import", '../data/addresses.csv');
    }

    function getQuery()
    {
        $sql = "REPLACE INTO address (id, area_id, name) VALUES(:id, :area_id, :name)";
        return $this->em->getConnection()->prepare($sql);
    }

    function mapRow(array $header, array $row): array
    {
        return [
            'id' => $row[$header['id']],
            'area_id' => $row[$header['areaId']],
            'name' => $row[$header['name']],
        ];
    }
}