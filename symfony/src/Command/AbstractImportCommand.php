<?php

namespace App\Command;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Statement as DriverStatement;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\LogicException as LogicExceptionAlias;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class AbstractImportCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * Title of command
     * @var string
     */
    protected $title;

    /**
     * ImportAreaCommand constructor.
     * @param EntityManagerInterface $em
     * @throws LogicExceptionAlias
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    /**
     * imports file and throws exception if hit any error during process
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Exception
     */
    protected function import(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getArgument('file');
        $progressBar = new ProgressBar($output);
        $progressBar->start();

        try {
            $handle = fopen($filename, "r");
            if (!$handle) {
                throw new \Exception("Can't open file: `$filename`");
            }

            $lineNo = 0;
            $header = null;
            $query = $this->getQuery();

            while (($row = fgetcsv($handle)) !== false) {
                if (!$header) {
                    $header = array_flip($row);
                    continue;
                }

                if (!$query->execute($this->mapRow($header, $row))) {
                    throw new \Exception("Error during import lineNo=$lineNo");
                } else {
                    $progressBar->advance();
                }

                $lineNo++;
            }

            // flush changes to the database
            $this->em->flush();
            fclose($handle);

            $progressBar->clear();
        } catch (\Exception $exception) {
            $progressBar->clear();
            throw $exception;
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title($this->title);

        try {
            $this->import($input, $output);
            $io->success('Import CSV data');
        } catch (\Exception $exception) {
            $io->error($exception->getMessage());
        }
    }

    /**
     * Returns statement to use for import each row from file
     * @return DriverStatement
     * @throws DBALException
     */
    abstract function getQuery();

    /**
     * Maps $row data using $header information as index->name mapping
     * @param array $header
     * @param array $row
     * @return array
     */
    abstract function mapRow(array $header, array $row): array;
}