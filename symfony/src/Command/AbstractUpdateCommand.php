<?php

namespace App\Command;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Statement as DriverStatement;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\LogicException as LogicExceptionAlias;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class AbstractUpdateCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * Title of command
     * @var string
     */
    protected $title;

    /**
     * ImportAreaCommand constructor.
     * @param EntityManagerInterface $em
     * @throws LogicExceptionAlias
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    /**
     * Request input data and update related
     * @param OutputInterface $output
     * @throws \Exception
     */
    protected function update(OutputInterface $output)
    {
        $progressBar = new ProgressBar($output);
        $progressBar->start();

        try {
            $query = $this->getQuery();

            foreach($this->getData() as $row) {
                if (!$query->execute($this->mapRow($row))) {
                    throw new \Exception("Error during updating data");
                } else {
                    $progressBar->advance();
                }

            }

            // flush changes to the database
            $this->em->flush();
            $progressBar->clear();
        } catch (\Exception $exception) {
            $progressBar->clear();
            throw $exception;
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title($this->title);

        try {
            $this->update($output);
            $io->success('Update data');
        } catch (\Exception $exception) {
            $io->error($exception->getMessage());
        }
    }

    /**
     * Returns input information that will be used as source for updating
     * @return array
     * @throws DBALException
     */
    abstract function getData(): array;

    /**
     * Returns statement to use for updating each row
     * @return DriverStatement
     * @throws DBALException
     */
    abstract function getQuery();

    /**
     * Return recalculated information based on $row data
     * @param array $row
     * @return array
     */
    abstract function mapRow(array $row): array;
}