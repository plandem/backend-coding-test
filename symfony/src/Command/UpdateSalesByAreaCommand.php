<?php

namespace App\Command;

class UpdateSalesByAreaCommand extends AbstractUpdateCommand
{
    protected $title = "Update sales change for areas";

    protected function configure()
    {
        $this
            ->setName('update:sales-by-area')
            ->setDescription('Updates sales change for areas');
    }

    function getQuery()
    {
        $sql = "REPLACE INTO saleByArea (area_id, fromDate, toDate, timespan, percentageChange) VALUES(:area_id, :fromDate, :toDate, :timespan, :percentageChange)";
        return $this->em->getConnection()->prepare($sql);
    }

    function getData():array
    {
        $connection = $this->em->getConnection();
        $connection->executeQuery("DELETE FROM saleByArea");
        return $connection->fetchAll(
            <<<SQL
SELECT
    area_id,
    periods.toDate,
    periods.fromDate,
    (SELECT price from sale WHERE area_id = periods.area_id AND date=periods.fromDate) AS firstPrice,
    (SELECT price from sale WHERE area_id = periods.area_id AND date=periods.toDate) AS lastPrice
FROM sale LEFT JOIN area ON area.id=sale.area_id INNER JOIN(
    SELECT area_id, MIN(date) AS fromDate, MAX(date) AS toDate FROM sale GROUP BY area_id
) AS periods USING(area_id) GROUP BY area_id
SQL
        );
    }

    function mapRow(array $row): array
    {
        return [
            'area_id' => $row['area_id'],
            'fromDate' => $row['fromDate'],
            'toDate' => $row['toDate'],
            'timespan' => date_diff(date_create($row['toDate']), date_create($row['fromDate']))->format('%a'),
            'percentageChange' => ($row['lastPrice'] - $row['firstPrice']) / $row['firstPrice'] * 100,
        ];
    }
}