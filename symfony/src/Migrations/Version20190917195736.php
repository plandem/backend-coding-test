<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190917195736 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE area (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE sale (id INT AUTO_INCREMENT NOT NULL, area_id INT NOT NULL, address_id INT NOT NULL, price INT NOT NULL, date DATE NOT NULL, PRIMARY KEY(id), FOREIGN KEY(area_id) REFERENCES area(id), FOREIGN KEY(address_id) REFERENCES address(id))');
        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, area_id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id), FOREIGN KEY(area_id) REFERENCES area(id))');

        $this->addSql('CREATE INDEX idx_address_area_id ON address(area_id)');
        $this->addSql('CREATE INDEX idx_sale_area_id ON sale(area_id)');
        $this->addSql('CREATE INDEX idx_sale_address_id ON sale(address_id)');

        $this->addSql('CREATE TABLE saleByArea (area_id INT NOT NULL, fromDate DATE NOT NULL, toDate DATE NOT NULL, timespan INT NOT NULL, percentageChange FLOAT NOT NULL, PRIMARY KEY(area_id, fromDate, toDate), FOREIGN KEY(area_id) REFERENCES area(id))');
        $this->addSql('CREATE INDEX idx_saleByArea_area_id ON saleByArea(area_id)');
        $this->addSql('CREATE INDEX idx_saleByArea_fromDate ON saleByArea(fromDate)');
        $this->addSql('CREATE INDEX idx_saleByArea_toDate ON saleByArea(toDate)');
        $this->addSql('CREATE INDEX idx_saleByArea_percentageChange ON saleByArea(percentageChange)');

        $this->addSql('CREATE TABLE saleByAddress (address_id INT NOT NULL, fromDate DATE NOT NULL, toDate DATE NOT NULL, timespan INT NOT NULL, percentageChange FLOAT NOT NULL, PRIMARY KEY(address_id, fromDate, toDate), FOREIGN KEY(address_id) REFERENCES address(id))');
        $this->addSql('CREATE INDEX idx_saleByAddress_address_id ON saleByAddress(address_id)');
        $this->addSql('CREATE INDEX idx_saleByAddress_fromDate ON saleByAddress(fromDate)');
        $this->addSql('CREATE INDEX idx_saleByAddress_toDate ON saleByAddress(toDate)');
        $this->addSql('CREATE INDEX idx_saleByAddress_percentageChange ON saleByAddress(percentageChange)');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE area');
        $this->addSql('DROP TABLE sale');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE saleByAddress');
        $this->addSql('DROP TABLE saleByArea');
    }
}
