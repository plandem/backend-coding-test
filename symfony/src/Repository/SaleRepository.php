<?php

namespace App\Repository;

use App\Entity\Sale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\DBALException;

/**
 * @method Sale|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sale|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sale[]    findAll()
 * @method Sale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sale::class);
    }

    /**
     * transform $sort with sorting values into SQL's ORDER segment
     * @param array|string $sort
     * @return string
     */
    protected function resolveSorting($sort = ""): string {
        $result = [];

        if (!is_array($sort)) {
            $sort = $sort !== "" ? explode(",", $sort) : [];
        }

        foreach($sort as $name) {
            if ($name === "") {
                continue;
            }

            $direction = "ASC";

            if($name[0] === '-') {
                $direction = "DESC";
                $name = substr($name, 1);
            }

            if ($name === "") {
                continue;
            }

            $result[] = "$name $direction";
        }

        return sizeof($result) ? " ORDER BY " . implode(",", $result) : "";
    }

    /**
     * @param array|string $sort
     * @return array
     * @throws DBALException
     */
    public function changesByArea($sort = ""): array
    {
        $conn = $this->getEntityManager()->getConnection();
        return $conn->executeQuery("SELECT name, timespan, percentageChange FROM saleByArea LEFT JOIN area ON saleByArea.area_id=area.id" . $this->resolveSorting($sort))->fetchAll();
    }

    /**
     * @param array|string $sort
     * @return array
     * @throws DBALException
     */
    public function changesByAddress($sort = ""): array
    {
        $conn = $this->getEntityManager()->getConnection();
        return $conn->executeQuery("SELECT name, timespan, percentageChange FROM saleByAddress LEFT JOIN address ON saleByAddress.address_id=address.id" . $this->resolveSorting($sort))->fetchAll();
    }
}
