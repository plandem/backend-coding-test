<?php

namespace App\Controller;

use App\Entity\Sale;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class SalesController extends AbstractController
{
    public function changeByAreaAction(Request $request)
    {
        try {
            $sales = $this->getDoctrine()->getRepository(Sale::class);
            return new JsonResponse(
                $sales->changesByArea($request->query->get('sort'))
            );
        } catch (\Exception $e) {
            return new JsonResponse([
                'error' => $e->getMessage(),
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function changeByAddressAction(Request $request)
    {
        try {
            $sales = $this->getDoctrine()->getRepository(Sale::class);
            $response = new JsonResponse(
                $sales->changesByAddress($request->query->get('sort'))
            );
            return $response;
        } catch (\Exception $e) {
            return new JsonResponse([
                'error' => $e->getMessage(),
            ], Response::HTTP_NOT_FOUND);
        }
    }
}