<?php

namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController
{
    public function indexAction()
    {
        return new Response(
<<<EOF
          <html>
            <body>
              <h3>Property.ca Backend Coding Test. Allowed routes:</h3>
              <ul>
                <li><a href="/sales/change-by-address">Change By Address</a></li>
                <li><a href="/sales/change-by-address?sort=percentageChange">Change By Address, ASC</a></li>
                <li><a href="/sales/change-by-address?sort=-percentageChange">Change By Address, DESC</a></li>
                <li><a href="/sales/change-by-area">Change By Area</a></li>
                <li><a href="/sales/change-by-area?sort=percentageChange">Change By Area, ASC</a></li>
                <li><a href="/sales/change-by-area?sort=-percentageChange">Change By Area, DESC</a></li>
              </ul>
            </body>
            </html>
EOF
        );
    }
}